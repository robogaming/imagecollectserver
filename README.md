# Team2556's ImageCollectServer
A server to collect data for training our ML models.
Made up of three parts: The backend web server, the
frontend web site, and a java app to access a Microsoft
LifeCam and take and upload images. (This ensures that
the image settings match those of what will eventually
run the trained, exported tflite model).

*The API & configuration is documented at the bottom of this file.*

## Plan and Completion

- [ ] Backend
    - [ ] Associate with some id / object
        - [ ] Images
            - [ ] Store images
            - [ ] Load images (random with < 3 votes on it, by id)
        - [ ] Number of balls
            - [ ] "Vote" (tally decisions) how many balls image contains
            - [ ] Load highest vote
        - [ ] Positional data
            - [ ] Load ball positional data
            - [ ] Store ball positional data (can store multiple)
            - [ ] "Vote" whether each piece of data is correct
            - [ ] Get data with >3 yes votes
        - [ ] Who has voted on this
            - [ ] Store whether you have voted (when this endpoint called store data collected from above in database)
            - [ ] Get if already voted
            - [ ] Lock out all other api functions for this image if already voted
    - [ ] Get a copy of all data
    - [ ] Authentication
- [ ] Frontend
    - [ ] Java App
        - [ ] Take pictures
        - [ ] Upload to server
    - [ ] Website
        - [ ] Password?
        - [ ] Load a random image
        - [ ] Check if already voted (if so load another image)
        - [ ] Vote on number of balls
        - [ ] No data available about ball positions
            - [ ] Select balls and upload the bounding box(es)
        - [ ] Data available
            - [ ] Vote on the positional data, or the alternative positional data
            - [ ] If vote no, provide alternative positional data
        - [ ] Load another image and repeat
    
## Configuration File
Located in `configs/config.yml` after starting for the first time.
```yaml
# -------------------------------
#     Functional settings
# -------------------------------

# Whether to use the clients' real IP or the value of X-Forwarded-For
proxied: false
# Include INFO logs?
verbose: true

# -------------------------------
#       Cosmetic settings
# -------------------------------

# Emojis in API endpoints, longer & more descriptive error messages
fun: true

# -------------------------------
#    Settings for security
# -------------------------------

# Hides the version string in /api
hideVersion: false
# Server name in /api (leave as "" to remove it)
serverName: "mini_1"
# Stop the server on an internal error (prevents people from messing with exploitable code)
errorStop: false
```

## API Routes


`GET` /some/route/that/doesnt/exist/ `Status 404`
```json5
{ // response
  "message": "🔌 Oops! There is nothing here!",
  "statuscode": 404,
  "error": true
}
```

`GET` /some/unhandled/error/ `Status 404`
```json5
{ // response
  "message": "🔌 Internal server error. Watch out - you may have discovered an exploit!",
  "statuscode": 500,
  "error": true
}
```

`GET` /some/route/without/authentication `Status 511`
```json5
{ // response
  "message": "🦠 Please sign in first.",
  "statuscode": 511,
  "error": true
}
```

`GET` /api/  `Status 200`
```json5
{ // response
  "message": "🌍 Hello world! Running API vX.X on server serverName",
  "statuscode": 200,
  "error": false
}
```

`POST` /api/images/create/ `Status 201`
```
( Request would be FormData of the image, and it must be a valid PNG )
```
```json5
{ // response
  "id": "uuid-of-the-created-image",
  "statuscode": 201,
  "error": false
}
```

`GET` /api/images/read/**:id**/ `Status 200 / Status 404`
```
( Would return the image )
```
```json5
{ // response
  "message": "🔌 That image doesn't exist!",
  "statuscode": 404,
  "error": true
}
```

`POST` /api/voting/ballnumber/update/**:id**/ `Status 202 / Status 400 / Status 404`
```json5
{ // request
  "number": 3, // if the user said there were 3 balls in the image
}
```
```json5
{ // response
  "message": "🗳 Your vote has been accepted",
  "statuscode": 200,
  "error": false
}
```
```json5
{ // response
  "message": "🗑 Your vote was unreasonable",
  "statuscode": 400,
  "error": true
}
```
```json5
{ // response
  "message": "🔌 You can't vote on nothing!",
  "statuscode": 404,
  "error": true
}
```
`GET` /api/voting/ballnumber/read/**:id**/ `Status 200 / Status 404`
```json5
{ // response
  "number": 3, // Most people voted for 3 balls in the image
  "statuscode": 200,
  "error": false
}
```
```json5
{ // response
  "message": "🔌 That vote data doesn't exist.",
  "statuscode": 404,
  "error": true
}
```

Hey you! This is the end of the API Documentation because I have to start
working on it. It isn't complete and only supports basic features. Later I
will add more once I get more done in code.