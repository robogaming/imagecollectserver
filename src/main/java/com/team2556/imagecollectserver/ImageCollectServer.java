package com.team2556.imagecollectserver;

import com.team2556.imagecollectserver.lib.Configurator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static spark.Spark.*;

public class ImageCollectServer {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final Configurator configurator = new Configurator("config.yml");

    ImageCollectServer() {
        port(1337);

        new APIRoutes(this);
        new FrontendRoutes(this);
    }

    public static void main(String[] args) {
        new ImageCollectServer();
    }

    public String versionNumber() {
        return "v0.1";
    }

    public Configurator getConfig() {
        return configurator;
    }
}
