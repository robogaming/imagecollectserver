package com.team2556.imagecollectserver.lib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

public class Configurator {
    private final Logger logger = LogManager.getLogger(this.getClass());
    File configFile;
    String configName;
    Yaml yaml = new Yaml();
    Map<String, Object> properties = new HashMap<>();
    WatchKey watchKey;

    public Configurator(String fileName) {
        configFile = new File("./configs/" + fileName);
        configName = fileName;

        reloadConfig();

        try {
            Path thisDir = FileSystems.getDefault().getPath("./configs");
            WatchService watcher = FileSystems.getDefault().newWatchService();
            watchKey = thisDir.register(watcher, ENTRY_MODIFY, ENTRY_DELETE);
        } catch (IOException e) {
            logger.warn("File watcher not available, auto config reload won't work.");
            e.printStackTrace();
        }
    }

    public void reloadConfig() {
        if (!configFile.exists()) {
            logger.warn("Config doesn't exist, loading default. This might be bad depending on your setup.");

            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream defaultConfig = classloader.getResourceAsStream(configName);
            try {
                try {
                    Files.createDirectory(FileSystems.getDefault().getPath("./configs"));
                } catch (FileAlreadyExistsException e) {
                    logger.info("./configs directory already exists, this may be bad unless you intended to force the default config.");
                }
                configFile.createNewFile();

                if (defaultConfig == null) {
                    logger.error("JAR is corrupt! Default config file does not exist!");
                    System.exit(1);
                }
                byte[] buffer = new byte[defaultConfig.available()];
                defaultConfig.read(buffer);
                OutputStream outStream = new FileOutputStream(configFile);
                outStream.write(buffer);
            } catch (Exception e) {
                logger.error("Error creating default config: " + e.getMessage());
                e.printStackTrace();
                System.exit(1); // If the config cant load the server can't run
            }
        }

        try {
            InputStream targetStream = new FileInputStream(configFile);
            properties = yaml.load(targetStream);
        } catch (FileNotFoundException e) {
            logger.error("Error loading config: " + e.getMessage());
            e.printStackTrace();
            System.exit(1); // If the config cant load the server can't run
        }
    }



    public Object get(String prop) {
        if (watchKey.pollEvents().size() > 0) {
            logger.info("Config changes have been reloaded.");
            reloadConfig();
        }

        if (properties.containsKey(prop)) {
            return properties.get(prop);
        } else {
            logger.error("Bad configuration: Option \"" + prop + "\" doesn't exist!");
            System.exit(1);
            return null;
        }
    }
}
