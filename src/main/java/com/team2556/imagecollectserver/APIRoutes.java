package com.team2556.imagecollectserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Date;

import static spark.Spark.*;

public class APIRoutes {
    private final Logger logger = LogManager.getLogger(this.getClass());

    APIRoutes(ImageCollectServer server) {
        path("/api", () -> {
            before("/*", (req, res) -> logger.info("API Route " + req.pathInfo() + " accessed by " + req.ip() + " on " + (new Date().toString())));

            get("", (req, res) -> {
                res.status(200);
                res.type("application/json");
                if ((boolean)server.getConfig().get("fun")) {
                    return "{\n" +
                            "  \"message\": \"\uD83C\uDF0D Hello world!" +
                            ((boolean)server.getConfig().get("hideVersion")  ? "" : " Running API " + server.versionNumber()) +
                            (server.getConfig().get("serverName").equals("") ? "" : " on server " + server.getConfig().get("serverName")) +
                            "\",\n" +
                            "  \"statuscode\": 200,\n" +
                            "  \"error\": false\n" +
                            "}";
                } else {
                    return "{\n" +
                            "  \"message\": \"Server operational. " +
                            ((boolean)server.getConfig().get("hideVersion")  ? "" : " API " + server.versionNumber()) +
                            (server.getConfig().get("serverName") == "" ? "" : " " + server.getConfig().get("serverName")) +
                            "\",\n" +
                            "  \"statuscode\": 200,\n" +
                            "  \"error\": false\n" +
                            "}";
                }
            });



            path("/images", () -> {
                get("/create", (req, res) -> {
                    throw new NullPointerException();
                });
            });

            path("/voting", () -> {

            });
        });

        exception(Exception.class, (err, req, res) -> {
            logger.error(err.getClass().toString() + " " + err.getMessage() + " Stack trace:");
            Arrays.stream(err.getStackTrace()).iterator().forEachRemaining((item) -> {logger.error("\t" + item);});
            res.status(500);
            res.type("application/json");
            if ((boolean)server.getConfig().get("fun")) {
                res.body("{\n" +
                        "  \"message\": \"\uD83D\uDD0C Internal server error. Watch out - you may have discovered an exploit!\",\n" +
                        "  \"statuscode\": 500,\n" +
                        "  \"error\": true\n" +
                        "}");
            } else {
                res.body("{\n" +
                        "  \"message\": \"Internal server error.\",\n" +
                        "  \"statuscode\": 500,\n" +
                        "  \"error\": true\n" +
                        "}");
            }
            if ((boolean)server.getConfig().get("errorStop")) {
                logger.error(() -> {
                    System.exit(1);
                    return "Exiting program after logging finished.";
                });
            }
        });

        after("/*", (req, res) -> {
            if (res.body() == null) {
                res.status(404);
                res.type("application/json");
                if ((boolean)server.getConfig().get("fun")) {
                    res.body("{\n" +
                            "  \"message\": \"\uD83D\uDD0C Oops! There is nothing here!\",\n" +
                            "  \"statuscode\": 404,\n" +
                            "  \"error\": true\n" +
                            "}");
                } else {
                    res.body("{\n" +
                            "  \"message\": \"This page does not exist.\",\n" +
                            "  \"statuscode\": 404,\n" +
                            "  \"error\": true\n" +
                            "}");
                }
            }
        });

        logger.info("API routes added.");
    }
}
