package com.team2556.imagecollectserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static spark.Spark.*;

public class FrontendRoutes {
    private final Logger logger = LogManager.getLogger(this.getClass());

    FrontendRoutes(ImageCollectServer server) {
        logger.info("Frontend routes added.");
    }
}
